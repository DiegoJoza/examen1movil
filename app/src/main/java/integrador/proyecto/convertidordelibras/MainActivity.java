package integrador.proyecto.convertidordelibras;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText cajaPeso;
    Button convertir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cajaPeso =findViewById(R.id.introduccirPeso);
        convertir = findViewById(R.id.convertirPe);

        convertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Main2Activity.class);
                intent.putExtra("peso", cajaPeso.getText().toString());
                Toast toast = Toast.makeText(getApplicationContext(), "Regresa a la pantalla anterior para otra conversión", Toast.LENGTH_LONG);
                toast.show();
                startActivity(intent);
            }
        });
    }
}
