package integrador.proyecto.convertidordelibras;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView textResultado;
    Double gramo = 453.592;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textResultado = findViewById(R.id.textResulado);

        String peso = getIntent().getExtras().getString("peso");
        Double entrada = Double.parseDouble(peso);
        Double salida = entrada * gramo;
        String finals = Double.valueOf(salida).toString();
        textResultado.setText(finals +  " gramos, quiero 10");
    }
}
